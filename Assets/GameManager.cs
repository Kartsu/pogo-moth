﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    AudioSource audioSource;
    public static GameManager instance = null;
    public bool isPowerUpBeingUsed = false;
    public GameObject ControlsWindow;
    public int level = 0;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            //FMODUnity.RuntimeManager.LoadBank("Master");
            audioSource = GetComponent<AudioSource>();
            audioSource.Play();
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        isPowerUpBeingUsed = false;
        LouderBGM();
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        isPowerUpBeingUsed = false;
        LouderBGM();
        level += 1;
    }

    public void SoftenBGM()
    {
        audioSource.volume = 0.1f;
    }

    public void LouderBGM()
    {
        audioSource.volume = 0.3f;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            ControlsWindow.SetActive(!ControlsWindow.activeSelf);
        }
        //print(FMODUnity.RuntimeManager.HasBankLoaded("Master"));
        //print(FMODUnity.RuntimeManager.HasBanksLoaded);
    }
}
