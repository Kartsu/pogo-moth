﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingBoxObstacle : MonoBehaviour
{
    bool hasDied = false;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            if (!hasDied)
            {
                hasDied = true;
                collision.GetComponent<Movement>().Die();
            }
        }
    }
}
