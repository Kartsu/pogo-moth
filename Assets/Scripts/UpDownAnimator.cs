﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownAnimator : MonoBehaviour
{

    bool moveDown = true;
    public float moveAmount;
    public float frequency = 0.25f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Move", frequency, frequency);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Move()
    {
        if(moveDown)
        {
            moveDown = !moveDown;
            transform.position = new Vector2(transform.position.x, transform.position.y - moveAmount);
        }
        else
        {
            moveDown = !moveDown;
            transform.position = new Vector2(transform.position.x, transform.position.y + moveAmount);
        }
    }
}
