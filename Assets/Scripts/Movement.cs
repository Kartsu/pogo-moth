﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;

public class Movement : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody2D rb;

    bool wasGrounded = false;
    bool isGrounded = false;
    bool wasHeadbang = false;
    bool isHeadBang = false;
    public Vector2 bottomOffset, topOffset;
    public float collisionRadius = 0.25f;
    public LayerMask groundLayer;
    public float pogoAmplitude;
    public float horizontalSpeed;
    public float bounceWaitTime;
    public float minimumAmplitude;
    public float shakeAmplitudeMultiplier;
    float horizontalInput;
    float verticalInput;
    public ProximityCircle proximityCircle;
    public ParticleSystem glideParticle;
    public Transform RespawnPoint;

    IGroundChecker _groundChecker;

    public SpriteRenderer visuals;
    [Tooltip("How much the closeness of the proximitycircle factors in to jump amplitude and frequency")]
    public float proximityFactor;
    private CinemachineImpulseSource cinemachineImpulseSource;

    public ParticleSystem HeadBangParticle;

    [FMODUnity.EventRef]
    public string selectSound;
    FMOD.Studio.EventInstance bounceEvent;

    [FMODUnity.EventRef]
    public string headBangSound;
    FMOD.Studio.EventInstance headBangEvent;

    [SerializeField]
    UnityEvent m_Landed = default;
    public event UnityAction Landed
    {
        add { m_Landed.AddListener(value); }
        remove { m_Landed.RemoveListener(value); }
    }

    [SerializeField]
    UnityEvent m_Respawned = default;
    public event UnityAction Respawned
    {
        add { m_Respawned.AddListener(value); }
        remove { m_Respawned.RemoveListener(value); }
    }

    [SerializeField]
    UnityEvent m_Died = default;
    public event UnityAction Died
    {
        add { m_Died.AddListener(value); }
        remove { m_Died.RemoveListener(value); }
    }
    void OnDied()
    {
        m_Died.Invoke();
    }
    void OnRespawned()
    {
        m_Respawned.Invoke();
    }
    void OnLanded()
    {
        m_Landed.Invoke();
    }

    // Start is called before the first frame update
    void Start()
    {
        bounceEvent = FMODUnity.RuntimeManager.CreateInstance(selectSound);
        headBangEvent = FMODUnity.RuntimeManager.CreateInstance(selectSound);
        rb = GetComponent<Rigidbody2D>();
        cinemachineImpulseSource = GetComponent<CinemachineImpulseSource>();
        Landed += onLand;
        Respawned += RespawnPlayer;
        Died += OnDeath;
        OnRespawned();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateIsGrounded();
        GetInputs();
    }

    //Gets the inputs
    void GetInputs()
    {
        horizontalInput = 0;
        verticalInput = 0;
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            horizontalInput = 1.0f;
        }
        if (Input.GetAxisRaw("Horizontal") < 0)
        {
            horizontalInput = -1.0f;
        }
        if (Input.GetAxisRaw("Vertical") > 0)
        {
            verticalInput = 1.0f;
        }
        if (Input.GetAxisRaw("Vertical") < 0)
        {
            verticalInput = -1.0f;
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            Die();
        }
    }

    //Updates the visuals
    void UpdateVisuals()
    {
        if (horizontalInput == -1.0f)
        {
            visuals.flipX = true;
        }
        if (horizontalInput == 1.0f)
        {
            visuals.flipX = false;
        }
    }

    private void FixedUpdate()
    {
        UpdateVisuals();
        rb.velocity = new Vector2(horizontalInput * horizontalSpeed, rb.velocity.y);
        //rb.velocity.x = Mathf.Lerp(rb.velocity.x, horizontalInput * horizontalSpeed, )
        //rb.velocity = new Vector2(DOTween.To(() => rb.velocity.x), x => (horizontalInput * horizontalSpeed) = x))
    }


    public void UpdateIsGrounded()
    {
        //Check if grounded :D
        Collider2D collider = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);
        isGrounded = collider;
        if (wasGrounded == false && isGrounded)
        {
            OnLanded();
            if(collider.TryGetComponent(out VanishingPlatform vanishingPlatform))
            {
                vanishingPlatform.Vanish();
            }
        }
        

        isHeadBang = Physics2D.OverlapCircle((Vector2)transform.position + topOffset, collisionRadius, groundLayer);
        if(wasHeadbang == false && isHeadBang)
        {
            headBangEvent.start();
            HeadBangParticle.Play();
        }
        wasGrounded = isGrounded;
        wasHeadbang = isHeadBang;
    }

    //When the player touches the ground
    private void onLand()
    {
        StartCoroutine(WaitThenBounce());
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere((Vector2)transform.position + bottomOffset, collisionRadius);
        Gizmos.DrawWireSphere((Vector2)transform.position + topOffset, collisionRadius);
    }

    IEnumerator WaitThenBounce()
    {
        yield return new WaitForSeconds(bounceWaitTime);
        float normalisedProximity = proximityCircle.GetNormalisedProximity();
        bounceEvent.setParameterByName("Height", normalisedProximity * 5f);
        bounceEvent.start();
        rb.velocity = new Vector2(0, minimumAmplitude + pogoAmplitude * (normalisedProximity * proximityFactor));
        cinemachineImpulseSource.GenerateImpulse(normalisedProximity * shakeAmplitudeMultiplier);
    }

    void RespawnPlayer()
    {
        Time.timeScale = 1;
        transform.position = RespawnPoint.position;
    }

    public void Die()
    {
        OnDied();
    }

    void OnDeath()
    {
        Time.timeScale = 0;
    }
}
