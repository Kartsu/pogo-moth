﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PowerupBar : MonoBehaviour
{

    public Slider slider;
    public float powerupDuration;

    public void SetPowerUpDuration(float duration)
    {
        slider.maxValue = duration;
        slider.value = duration;
    }

    [SerializeField]
    UnityEvent m_PowerUpFinished = default;
    public event UnityAction PowerUpFinished
    {
        add { m_PowerUpFinished.AddListener(value); }
        remove { m_PowerUpFinished.RemoveListener(value); }
    }
    void OnPowerUpFinished()
    {
        m_PowerUpFinished.Invoke();
        GameManager.instance.isPowerUpBeingUsed = false;
        gameObject.SetActive(false);
    }

    public void SetTime(float time)
    {
        slider.value = Mathf.Max(slider.minValue,slider.value - time);
        if(slider.value == slider.minValue)
        {
            OnPowerUpFinished();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        SetPowerUpDuration(powerupDuration);
    }

    // Update is called once per frame
    void Update()
    {
        SetTime(Time.deltaTime);
    }
}
