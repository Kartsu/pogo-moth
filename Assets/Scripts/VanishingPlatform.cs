﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class VanishingPlatform : MonoBehaviour
{

    DOTweenAnimation fade;

    private void Start()
    {
        fade = GetComponent<DOTweenAnimation>();
    }
    // Start is called before the first frame update

    public void Vanish()
    {
        fade.DOPlay();
    }
}
