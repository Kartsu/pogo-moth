﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class PowerUp : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string selectSound;
    public FMOD.Studio.EventInstance soundEvent;

    [FMODUnity.EventRef]
    public string timeOutSound;
    public FMOD.Studio.EventInstance timeOutEvent;

    [SerializeField]
    UnityEvent m_Activated = default;
    public event UnityAction Activated
    {
        add { m_Activated.AddListener(value); }
        remove { m_Activated.RemoveListener(value); }
    }
    void OnActivated()
    {
        isActivated = true;
        m_Activated.Invoke();
        soundEvent.start();
    }

    public PowerupBar slider;
    public float powerUpDuration = 10.0f;
    public SpriteRenderer sr;
    public bool isActivated = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (!GameManager.instance.isPowerUpBeingUsed)
            {
                GameManager.instance.isPowerUpBeingUsed = true;
                slider.gameObject.SetActive(true);
                slider.powerupDuration = powerUpDuration;
                slider.SetPowerUpDuration(powerUpDuration);
                ActivatePowerUp();
                sr.enabled = false;
                OnActivated();
            }
        }
    }

    public abstract void ActivatePowerUp();

    public abstract void DeactivatePowerUp();

    private void Start()
    {
        soundEvent = FMODUnity.RuntimeManager.CreateInstance(selectSound);
        timeOutEvent = FMODUnity.RuntimeManager.CreateInstance(timeOutSound);
    }
}
