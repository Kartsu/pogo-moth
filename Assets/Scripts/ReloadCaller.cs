﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadCaller : MonoBehaviour
{

    public void Reload()
    {
        GameManager.instance.ReloadScene();
    }
}
