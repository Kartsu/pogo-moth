﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelDisplayer : MonoBehaviour
{

    TextMeshProUGUI textMesh;
    // Start is called before the first frame update
    void Start()
    {
        textMesh = GetComponent<TextMeshProUGUI>();
        textMesh.text = "Level " + GameManager.instance.level;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
