﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelCaller : MonoBehaviour
{

    public void NextLevel()
    {
        GameManager.instance.NextLevel();
    }
}
