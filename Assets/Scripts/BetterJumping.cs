﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterJumping : MonoBehaviour
{

    private Rigidbody2D rb;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    public Animator visualsAnimator;
    public ParticleSystem glideParticle;
    [FMODUnity.EventRef]
    public string glideSound;
    FMOD.Studio.EventInstance glideEvent;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        glideEvent = FMODUnity.RuntimeManager.CreateInstance(glideSound);
    }

    private void Update()
    {
        if (rb.velocity.y < 0 && Input.GetAxisRaw("Vertical") > 0 && Time.timeScale > 0)
        {
            visualsAnimator.SetBool("Flying", true);
            glideParticle.Play();
            FMOD.Studio.PLAYBACK_STATE playbackState;
            glideEvent.getPlaybackState(out playbackState);
            bool isPlaying = playbackState != FMOD.Studio.PLAYBACK_STATE.STOPPED;
            if(!isPlaying)
            {
                glideEvent.start();
            }
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier) * Time.deltaTime;
        }
        else
        {
            visualsAnimator.SetBool("Flying", false);
            glideParticle.Stop();
            glideEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }
}
