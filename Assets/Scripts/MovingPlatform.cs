﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform pos1, pos2;
    public float speed;
    public Transform startPos;
    public float yOffset;

    Vector2 NextPos;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = pos2.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(transform.position == pos1.position)
        {
            NextPos = pos2.position;
        }
        if(transform.position == pos2.position)
        {
            NextPos = pos1.position;
        }

        transform.position = Vector2.MoveTowards(transform.position, NextPos, speed * Time.deltaTime);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(pos1.position, pos2.position);
    }
}
