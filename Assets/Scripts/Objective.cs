﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Objective : MonoBehaviour
{

    bool isLevelCompleted = false;
    public GameObject completionCircle;

    [FMODUnity.EventRef]
    public string selectSound;
    FMOD.Studio.EventInstance soundEvent;

    [SerializeField]
    UnityEvent m_LevelCompleted = default;
    public event UnityAction LevelCompleted
    {
        add { m_LevelCompleted.AddListener(value); }
        remove { m_LevelCompleted.RemoveListener(value); }
    }
    void OnLevelCompleted()
    {
        m_LevelCompleted.Invoke();
        completionCircle.SetActive(true);
        soundEvent.start();
        Time.timeScale = 0;
    }

    void OnLevelSkipped()
    {
        m_LevelCompleted.Invoke();
        completionCircle.SetActive(true);
        //soundEvent.start();
        Time.timeScale = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        soundEvent = FMODUnity.RuntimeManager.CreateInstance(selectSound);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            OnLevelSkipped();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            if(isLevelCompleted == false)
            {
                OnLevelCompleted();
            }
        }
    }

    public void StartGame()
    {
        OnLevelSkipped();
    }
}
