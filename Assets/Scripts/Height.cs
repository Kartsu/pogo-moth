﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Height : MonoBehaviour
{

    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    public void Stretch()
    {
        animator.SetTrigger("stretch");
    }

    public void Squash()
    {
        animator.SetTrigger("squash");
    }
}
