﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlternatingPlatform : MonoBehaviour
{
    public GameObject RealTileMap;
    public GameObject PlaceholderTileMap;
    public bool isGreen = false;

    [FMODUnity.EventRef]
    public string switchSoundGreen;
    public FMOD.Studio.EventInstance GreenswitchEvent;

    [FMODUnity.EventRef]
    public string switchSoundBlue;
    public FMOD.Studio.EventInstance BlueswitchEvent;

    // Start is called before the first frame update
    void Start()
    {
        GreenswitchEvent = FMODUnity.RuntimeManager.CreateInstance(switchSoundGreen);
        BlueswitchEvent = FMODUnity.RuntimeManager.CreateInstance(switchSoundBlue);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Switch()
    {
        RealTileMap.SetActive(!RealTileMap.activeSelf);
        PlaceholderTileMap.SetActive(!PlaceholderTileMap.activeSelf);
        if(isGreen)
        {
            GreenswitchEvent.start();
        }
        else
        {
            BlueswitchEvent.start();
        }
        
    }
}
