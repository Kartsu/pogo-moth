﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezePowerup : PowerUp
{
    public ProximityCircle pc;
    float previousScaleFactor;
    public bool setCircleToNear = false;

    public override void ActivatePowerUp()
    {
        GameManager.instance.SoftenBGM();
        previousScaleFactor = pc.scaleFactor;
        pc.scaleFactor = 0f;
        if(setCircleToNear)
        {
            pc.transform.localScale = new Vector3(pc.minScale+0.01f, pc.minScale+0.01f, 1);
        }
    }

    public override void DeactivatePowerUp()
    {
        if (isActivated) {
            isActivated = false;
            GameManager.instance.LouderBGM();
            soundEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            timeOutEvent.start();
            pc.scaleFactor = previousScaleFactor;
            sr.enabled = true;
        }
    }

}
