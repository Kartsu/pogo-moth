﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class ProximityCircle : MonoBehaviour
{

    public float changeAmount;
    public float minScale;
    public float maxScale;
    public float scaleFactor;

    public Light2D light2D;
    public float startScale = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = new Vector3(startScale, startScale, 1);
        light2D = GetComponent<Light2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float newScale = Mathf.Clamp(transform.localScale.x + changeAmount * scaleFactor, minScale, maxScale);
        transform.localScale = new Vector3(newScale, newScale, 1);
        if(newScale == minScale || newScale == maxScale && scaleFactor != 0)
        {
            scaleFactor *= -1;
        }
    }

    // 0 is near, 1 is far
    public float GetNormalisedProximity() {
        return transform.localScale.x / maxScale;
    }
}
